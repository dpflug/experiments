(defun char-1- (in-char)
  (code-char (1- (char-code in-char))))

(defun char-1+ (in-char)
  (code-char (1+ (char-code in-char))))

(defmacro run-command-from-string (command &rest args)
  `(,command ,@args))


