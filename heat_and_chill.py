import math

def hi1(Tf, RH):
    """Takes the temperature in F and humidity as a percentage and returns the current heat index using method 1"""
    return -42.379 + 2.04901523 * Tf + 10.14333127 * RH + -0.22475541 * Tf * RH + -6.83783e-3 * Tf ** 2 + -5.481717e-2 * RH ** 2 + 1.22874e-3 * Tf ** 2 * RH + 8.5282e-4 * Tf * RH ** 2 + -1.99e-6 * Tf ** 2 * RH ** 2

def aat(Tf, RH, wskmh):
    """Takes the temperature in F and humidity as a percentage and returns the current heat index using method 1"""
    wsms = wskmh / 3600 * 1000
    Ta = (Tf - 32) * 5 / 9
    e = RH / 100 * 6.105 * math.exp(17.27 * Ta / (237.7 + Ta))
    AT = Ta + 0.348 * e - 0.7 * wsms
