(defpackage #:cl-gtp
  (:use :cl :asdf :f-underscore-system))

(in-package #:cl-gtp
	    :name "cl-gtp"
	    :description
"Parse Go Text Protocol. Optionally create SGF files from the results.")
