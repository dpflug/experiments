from Bot import trigger,consume,send,interact


class Alarms
   alarmdict = {}

   def add(where, when, what):
       # Do stuff. Add it to the dict, or whatever struct is being used.

    @trigger("^!alarm (.*)\#(.*)")
    @consume  # Nothing else is to use these messages.
    def set_alarm(who, where, when, what):  # Using positional arguments. Named is fine, too. I don't really care.
        time = alarms.add(where, alarm, message)
        send(who, "Alarm set for {}".format(time))

    @tick(60)  # Run every 60 seconds
    @interact  # Allow other plugins to alter/interact with this output
    def check_alarms(time):
        if time in alarmdict:
            for where in alarmdict[time]:
                for msg in where:
                    send(where, msg)

# Having both @consume and @interact is a bit redundant. We probably only need one or the other.
