#from Bot import trigger, send
import re

t_regex = "[-0-9.]+[FC]"

#@trigger(message=t_regex)
def temperature_converter(source, message):
    send(source, map(convfunc, re.split(t_regex, message)))

def convfunc(msg_part):
    if re.match(t_regex, msg_part):
        tempconv(msg_part)
    else:
        return msg_part

def tempconv(deg):
    if deg[len(deg) - 1] == "F":
        return FtoC(float(deg[:len(deg) - 2]))
    else:
        return CtoF(float(deg[:len(deg) - 

def CtoF(degC):
    return degC * 9 / 5 + 32

def FtoC(degF):
    return (degF - 32) * 5 / 9
